import { Article } from "../types"
import styles from "./Card.module.css"

const MAX_LENGTH = 80
const ELLPISES = "..."

const truncateTitle = (title: string) => {
  if (title.length > MAX_LENGTH - ELLPISES.length) {
    return title.slice(0, MAX_LENGTH) + ELLPISES
  } else {
    return title
  }
}

export const Card = (props: { article: Article }) => {
  const article: Article = props.article
  return (
    <a href={article.url} className={styles.card} target="_blank">
      <div>{truncateTitle(article.title)}</div>
    </a>
  )
}
