import { useEffect, useState } from "react"
import { useGetTopHeadlinesQuery } from "../../services/news"
import { Card } from "./Card"
import Spinner from "../../assets/LoadingSpinner.svg"
import styles from "./News.module.css"
import { Error } from "./Error"
import { useSelector } from "react-redux"
import { getApitokenFromState } from "../constants"

export const News = () => {
  const apiToken = useSelector(getApitokenFromState)
  const { data, error, isLoading } = useGetTopHeadlinesQuery(apiToken)

  return (
    <div>
      {isLoading ? <img src={Spinner} className={styles.spinner} /> : null}
      {error && <Error error={error} />}
      {data
        ? data.articles.map((article, i) => {
            return <Card key={i} article={article} />
          })
        : null}
    </div>
  )
}
