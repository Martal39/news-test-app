import { SerializedError } from "@reduxjs/toolkit"
import { FetchBaseQueryError } from "@reduxjs/toolkit/dist/query"

export const Error = (props: {
  error: FetchBaseQueryError | SerializedError | undefined
}) => {
  const error = props.error
  let message = ""
  if (error) {
    if ("status" in error) {
      // you can access all properties of `FetchBaseQueryError` here
      if (error.status === 401) {
        message = "The key is invalid!"
      }
    } else {
      // you can access all properties of `SerializedError` here
      message = "Somehing went wrong!"
    }
  }

  return <div>Error! {message}</div>
}
