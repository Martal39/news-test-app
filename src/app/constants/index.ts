import { RootState } from "../store"

export const ACTIONS = {
  LOG_IN: "LOG_IN",
  LOG_OUT: "LOG_OUT",
  SET_API_TOKEN: "SET_API_TOKEN",
}

export const LOGIN_REDUCER = "LOGIN_REDUCER"
export const API_REDUCER = "API_REDUCER"

export const getLoggedInState = (state: RootState) => state[LOGIN_REDUCER]
export const getApitokenFromState = (state: RootState) =>
  state[API_REDUCER]?.token

export const PATHS = {
  NEWS: "/news",
  INDEX: "/",
  EMPTY: "",
}
