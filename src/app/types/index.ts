export interface Article {
  author?: string
  content: string
  description: string
  publishedAt: string
  source: Source
  title: string
  url: string
}

export interface Source {
  id: string
  name: string
}

export interface ArticleQuery {
  articles: Array<Article>
}
