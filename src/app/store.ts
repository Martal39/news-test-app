import { Action, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit"
import { newsApi } from "../services/news"
import { ACTIONS, API_REDUCER, LOGIN_REDUCER } from "./constants"

const apiReducer = (state = { token: "" }, action: Action) => {
  switch (action.type) {
    case ACTIONS.SET_API_TOKEN:
      return { token: action.payload }

    default:
      return state
  }
}

const loginReducer = (
  state = { isLoggedIn: false },
  action: { type: string },
) => {
  switch (action.type) {
    case ACTIONS.LOG_IN:
      return { isLoggedIn: true }
    case ACTIONS.LOG_OUT:
      return { isLoggedIn: false }

    default:
      return state
  }
}

export const store = configureStore({
  reducer: {
    [newsApi.reducerPath]: newsApi.reducer,
    [LOGIN_REDUCER]: loginReducer,
    [API_REDUCER]: apiReducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(newsApi.middleware)
  },
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
