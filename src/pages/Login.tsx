import { useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { ACTIONS, PATHS } from "../app/constants"
import { useState } from "react"
import styles from "./Login.module.css"

export const Login = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [email, setEmail] = useState("")
  const [apiToken, setApiToken] = useState("")
  return (
    <div>
      <h1 className={styles.title}>Login</h1>
      <form
        className={styles.form}
        onSubmit={(e) => {
          e.preventDefault()
          dispatch({ type: ACTIONS.LOG_IN })
          dispatch({ type: ACTIONS.SET_API_TOKEN, payload: apiToken })
          navigate(PATHS.NEWS)
        }}
      >
        <label htmlFor="email">
          Enter an email address (this won't be stored anywhere):
        </label>
        <input
          id="email"
          type="email"
          value={email}
          className={styles.textInput}
          onChange={(e) => {
            setEmail(e.target.value)
          }}
        />
        <label htmlFor="apiToken">Enter API Token:</label>
        <input
          type="text"
          id="apiToken"
          value={apiToken}
          className={styles.textInput}
          onChange={(e) => setApiToken(e.currentTarget.value)}
          required
        />
        <button className={styles.button}>Log in!</button>
      </form>
    </div>
  )
}
