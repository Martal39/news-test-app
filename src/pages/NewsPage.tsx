import { News } from "../app/components/News"
import Logo from "../assets/Logo.svg"
import styles from "./NewsPage.module.css"

export const NewsPage = () => {
  return (
    <section>
      <div className={styles.header}>
        <header className={styles.title}>The News Desk</header>
        <img src={Logo} className={styles.logo} alt="logo" />
      </div>
      <News />
    </section>
  )
}
