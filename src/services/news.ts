import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { ArticleQuery } from "../app/types"

const NEWS_API = "https://newsapi.org/v2/"

const createTopHeadlinesPath = (apiKey: String) =>
  `top-headlines?country=us&apiKey=${apiKey}`

export const newsApi = createApi({
  reducerPath: "news",
  baseQuery: fetchBaseQuery({ baseUrl: NEWS_API }),
  endpoints: (builder) => ({
    getTopHeadlines: builder.query<ArticleQuery, String>({
      query: createTopHeadlinesPath,
    }),
  }),
})

export const { useGetTopHeadlinesQuery } = newsApi
