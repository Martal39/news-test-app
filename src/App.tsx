import {
  Routes,
  Route,
  Outlet,
  Link,
  Navigate,
  useNavigate,
} from "react-router-dom"
import { Login } from "./pages/Login"
import { NewsPage } from "./pages/NewsPage"
import styles from "./App.module.css"
import { useDispatch } from "react-redux"
import { useAppSelector } from "./app/hooks"
import { ACTIONS, PATHS, getLoggedInState } from "./app/constants"

function App() {
  const state = useAppSelector(getLoggedInState)

  return (
    <Routes>
      <Route path={PATHS.INDEX} element={<Layout />}>
        <Route index path={PATHS.EMPTY} element={<Login />} />
        <Route
          path={PATHS.NEWS}
          element={
            <ProtectedRoute loggedIn={state.isLoggedIn}>
              <NewsPage />
            </ProtectedRoute>
          }
        />
        <Route index path="*" element={<Login />} />
      </Route>
    </Routes>
  )
}

const ProtectedRoute = (props: {
  loggedIn: boolean
  children: JSX.Element
}): JSX.Element => {
  if (!props.loggedIn) {
    return <Navigate to={"/login"} replace />
  }

  return props.children
}

function Layout() {
  const dispatch = useDispatch()
  const state = useAppSelector(getLoggedInState)
  const navigate = useNavigate()

  return (
    <div className={styles.layout}>
      <nav className={styles.nav}>
        <ul>
          <li
            onClick={() => {
              if (state.isLoggedIn) {
                dispatch({ type: ACTIONS.LOG_OUT })
              } else {
                navigate(PATHS.INDEX)
              }
            }}
          >
            <a href="">
              {state.isLoggedIn
                ? "You are signed in. Click to log out."
                : "Login page"}
            </a>
          </li>
        </ul>
      </nav>

      <div className={styles.main}>
        <Outlet />
      </div>
    </div>
  )
}

export default App
