import { act, fireEvent, render, waitFor } from "@testing-library/react"
import { Provider } from "react-redux"
import { store } from "./app/store"
import App from "./App"
import { MemoryRouter } from "react-router-dom"

test("renders login", () => {
  const { getByText } = render(
    <Provider store={store}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>,
  )

  expect(getByText(/Login page/i)).toBeInTheDocument()
})

test("logs in and fetches data", async () => {
  const { getByLabelText, getByText } = render(
    <Provider store={store}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>,
  )

  const email = getByLabelText(/Enter an email address/)
  const apiToken = getByLabelText(/Enter API Token:/)
  const logIn = getByText(/Log in!/)

  fireEvent.change(email, { target: { value: "test@example.com" } })
  fireEvent.change(apiToken, { target: { value: "test" } })
  act(() => {
    fireEvent.click(logIn)
  })

  await waitFor(() => {
    expect(getByText(/Test Title/i)).toBeInTheDocument() // Test Title defined in Test Setup
  })
})
