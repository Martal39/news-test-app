/// <reference types="vitest/globals" />
import "@testing-library/jest-dom"
import { setupServer } from "msw/node"
import { rest } from "msw"
import "whatwg-fetch"

const articles = {
  articles: [
    {
      source: {
        id: "abc-news",
        name: "ABC News",
      },
      author: "test",
      title: "Test Title",
      url: "example.com",
    },
  ],
}

const server = setupServer(
  rest.get("https://newsapi.org/v2/top-headlines", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(articles))
  }),
)

// Start server before all tests
beforeAll(() => server.listen())

//  Close server after all tests
afterAll(() => server.close())

// Reset handlers after each test `important for test isolation`
afterEach(() => server.resetHandlers())
