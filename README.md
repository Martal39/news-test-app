# News App

Test assignment.

Uses the Vite Template Redux, see below for details. To run, do `npm install` and `npm run dev`.

Built using Node 14.17.4.

## Observations and notes

This took me about 12 hours to do. This was my first time using Redux Toolkit and RTK Query (saw it as a good way
to gain some familiarity). I have used TypeScript in the past, but only in limited hobby projects and sadly never in
production, so expect some weirdness / sub-optimal solutions.

Testing was a huge pain! Turns out there is some issue with `fetch` not being present in `jsdom`, took me a good 2-3 hours
to figure out.

I also made some assets! I hope you enjoy.

I will specify that, from a product point of view, there are numerous improvements to make. For example, the requirements don't
make any mention of there being persistence. As I was very short on time, I did not go above and beyond here, but in a real work scenario,
I'd have questions to the PM about what we want our UX to be like. Plus, I was very hesitant on just collecting someone's email, again,
would need to check with PM about restrictions in regard to privacy, etc.

I was also much less strict with my commits.

### vite-template-redux

Uses [Vite](https://vitejs.dev/), [Vitest](https://vitest.dev/), and [React Testing Library](https://github.com/testing-library/react-testing-library) to create a modern [React](https://react.dev/) app compatible with [Create React App](https://create-react-app.dev/)

```sh
npx degit reduxjs/redux-templates/packages/vite-template-redux my-app
```

## Scripts

- `dev`/`start` - start dev server and open browser
- `build` - build for production
- `preview` - locally preview production build
- `test` - launch test runner
